from django.urls import path
from . import views

app_name = 'car_plates_a'

urlpatterns = [
    path('', views.index, name='index'),
    path('<int:plate_id>/', views.details, name='details'),
    path('<int:plate_id>/delete_plate', views.delete_plate, name='delete_plate'),
    path('create_plate/', views.create_plate, name='create_plate'),
    path('<int:plate_id>/update_plate/', views.update_plate, name='update_plate'),
    # path('update_plate_post/<int:plate_id>/', views.update_plate_post, name='update_plate_post'),
]