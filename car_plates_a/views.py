from django.shortcuts import render, redirect
from django.db import IntegrityError
from .models import CarPlateA
import re

# main page
def index(request):
	car_plates = CarPlateA.objects.all() # collect all car plates
	context = {'plates_list': car_plates} # pass  them thro context
	return render(request, 'car_plates_a/index.html', context)

# details page (note necesary at all)
def details(request, plate_id):
	car_plate = CarPlateA.objects.get(id=plate_id)
	context = {'plate': car_plate}
	return render(request, 'car_plates_a/details.html', context)

# create plate view and post action
def create_plate(request):
	# If method is POST (FORM submit)
	if request.method == "POST":
		# Retrieve data from post method
		plate_number = request.POST['plate_number']
		plate_owner = request.POST['plate_owner']
		# context will be passed so that data woudnt be deleted if validation would fail
		context = {
				"plate_number": plate_number,
				"plate_owner": plate_owner
			}

		err_message = ""

		# plate number and owner validation
		if len(plate_number) > 6 or len(plate_number) < 1:
			err_message += "Car plate length must be between 1 and 6. "
		# check fo valid characters
		elif not re.match("^[a-zA-Z0-9_]*$", plate_number):
			err_message += "Car plate must contain only letters and number. "

		if err_message:
			context["err_message"] = err_message
			return render(request, 'car_plates_a/create_plate.html', context)

		new_car_plate = CarPlateA(plate_owner=plate_owner, plate_number=plate_number)
		# check 
		try:
			new_car_plate.save()
		except IntegrityError as e:
			err_message += "Car Plate probably already exists. "
			context["err_message"] = err_message
			return render(request, 'car_plates_a/create_plate.html', context)


		return redirect('/car_plates_a')
	else:
		return render(request, 'car_plates_a/create_plate.html')

def delete_plate(request, plate_id):
	CarPlateA.objects.filter(id=plate_id).delete()
	car_plates = CarPlateA.objects.all()
	context = {'plates_list': car_plates}
	return render(request, 'car_plates_a/index.html', context)

def update_plate(request, plate_id):
	if request.method == "POST":
		car_plate = CarPlateA.objects.get(id=plate_id)
		new_plate_number = request.POST['plate_number']
		new_plate_owner = request.POST['plate_owner']
		car_plate.plate_number = new_plate_number
		car_plate.plate_owner = new_plate_owner

		try:
			car_plate.save()
		except IntegrityError as e:
			context = {'car_plate': car_plate, "err_message": "Car Plate probably already exists!"}
			return render(request, 'car_plates_a/update_plate.html', context)

		car_plate.save()

		return redirect('/car_plates_a')
	else:
		car_plate = CarPlateA.objects.get(id=plate_id)
		context = {
			'car_plate': car_plate,
		}
		return render(request, 'car_plates_a/update_plate.html', context)
