from django.db import models


class CarPlateA(models.Model):

    plate_number = models.CharField(max_length=6, db_index=True,
        null=False, unique=True)
    plate_owner = models.CharField(max_length=64, db_index=True,
        null=False, default="Paulius Azubalis")
    create_date = models.DateField(auto_now=True)
    
    def __str__(self):
        return self.plate_number