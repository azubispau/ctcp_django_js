$( document ).ready(function() {

	// valid simbols
	var valid_input = /^[0-9a-zA-Z]+$/;

	// checking values on keyup to give color for input
    $(".input_plate_number").keyup(function() {
    	check_inpute_vals($(this), is_plate=true);
    });

    $(".input_plate_owner").keyup(function() {
    	check_inpute_vals($(this), is_plate=false, is_owner=true);
    });

    // function that check if input is valid
    function check_inpute_vals(input, is_plate=false, is_owner=false) {
    	var input_val = input.val();
    	// is it is a plate number then check length for 6 if it's a name check vall for 64
    	if(
            ((input_val.length > 64 && is_owner) || (input_val.length > 6 && is_plate)) ||
            (!input_val.replace(/\s/g, "").match(valid_input))
            ){
    		input.addClass("bad_input");
    		return false;
    	}
    	else if (input.hasClass('bad_input')){
    		input.removeClass("bad_input");
    		return false;
    	}
    	return true; // Field Validations
	}

    


});