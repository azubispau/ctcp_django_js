from django.apps import AppConfig


class CarPlatesAConfig(AppConfig):
    name = 'car_plates_a'
