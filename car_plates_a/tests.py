from django.test import TestCase
from .models import CarPlateA
# Create your tests here.

class CarPlateATestCase(TestCase):
	"""CarPlate test casees"""
	def setUp(self):
		CarPlateA.objects.create(plate_number="PUA689", plate_owner="Greta Rokaite")
		CarPlateA.objects.create(plate_number="LTU098", plate_owner="Justinas Mankus")

	def update_car_plate(self):
		plate1 = CarPlateA.objects.get(plate_number="PUA689")
		plate1.plate_number = "PUA685"
		plate1.save()
		plate2 = CarPlateA.objects.get(plate_number="LTU098")
		plate2.plate_number = "PUA685"
		plate2.save()

	def remove_car_plate(slef):
		CarPlateA.objects.get(plate_number="PUA685").delete()
		CarPlateA.objects.get(plate_number="LTU094").delete()
		