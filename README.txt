# Git remote : git clone https://enigmq@bitbucket.org/enigmq/ctcp_django_js.git
# pull/push password: WrX2FJdWeVTSq66B8nFP

# Note: before pulling project you should probably create virtual env.
# How to Create virtual env:
# open terminal, head to directory that you wish your virtual env to appear at, and type:
	virtualenv ltcp
# change directory to ltcp and type:
	source bin/activate
# this will activate virtual environment
# Install django by typing pip3 install django (if you dont have django. If you dont have pip you might want to install it will sudo apt-get install pip)
# Let's Create another directory:
	mkdir ltcp
	cd ltcp
# Now lets initialize directory as git remote by typing:
	git init
# Add remote: (not nessesary)
	git remote add origin https://enigmq@bitbucket.org/enigmq/ctcp_django_js.git
# Check your remote list:
	git remote -v
# Now we can pull:
	git pull https://enigmq@bitbucket.org/enigmq/ctcp_django_js.git
# Enter Password:
	WrX2FJdWeVTSq66B8nFP

# After succesful pull change directory one more time:
	cd ltcp
# Now we can launch our project with command:
	python3 manage.py runserver

# after launching project go to: http://127.0.0.1:8000/car_plates_a/ to view List or car plates.
# If you want to Create new car plate click Create Plate, Enter Plate Number and Plate owner and Click submit.
# If you wish to delete car plate click button Remove next to plate that you wish to delete.
# If you wish to update car plate click button update. Fill form with new values and click submit.

# Project has superuser for admin access:
	http://127.0.0.1:8000/admin/
	username : admin
	password : adminadmin

# Project is built with at that time latest version: django 2.0.3
# by Paulius Azubalis
# paulius.azubalis@gmail.com